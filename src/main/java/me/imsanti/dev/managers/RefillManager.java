package me.imsanti.dev.managers;

import me.imsanti.dev.managers.config.DataFile;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.LocationUtils;
import me.imsanti.dev.utils.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.*;

public class RefillManager {

    private final DataFile dataFile;
    private final MessagesFile messagesFile;
    private static final Map<Location, String> signs = new HashMap<>();

    public RefillManager(DataFile dataFile, MessagesFile messagesFile) {
        this.dataFile = dataFile;
        this.messagesFile = messagesFile;
    }
    public void createRefill(Player player, String refillName, int size, String materialName, boolean hasDataValue, int dataValue, int itemsPerRefill) {
        Location location = player.getTargetBlock((HashSet<Byte>) null, 1).getLocation();
        FileConfiguration config = dataFile.getConfig();
        if(refillExists(location)) {
            player.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("refill-already-exists")));
            return;
        }

        if(!PlayerUtils.isLookingSign(player)) {
            player.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("no-looking-sign")));
            return;
        }

        player.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("sign-created")));
        signs.put(location, refillName);
        if(hasDataValue) {
            LocationUtils.writeSign(location, config.createSection("signs." + refillName), size, materialName, true, dataValue, itemsPerRefill);

        }else {
            LocationUtils.writeSign(location, config.createSection("signs." + refillName), size, materialName, false, 0, itemsPerRefill);
        }

        dataFile.saveConfigFile(config);
    }

    public void deleteRefill() {

    }

    public boolean refillExists(Location location) {
        return signs.containsKey(location);
    }

    public void loadRefills() {
        if(dataFile.getConfig() == null) return;
        if(dataFile.getConfig().getConfigurationSection("signs") == null) return;
        if(dataFile.getConfig().getConfigurationSection("signs").getKeys(false) == null) return;

        for(final String key : dataFile.getConfig().getConfigurationSection("signs").getKeys(false)) {
            if(dataFile.getConfig().getConfigurationSection("signs." + key) == null) continue;

            signs.put(LocationUtils.readLocation(dataFile.getConfig().getConfigurationSection("signs." + key)), key);
        }

        Bukkit.getConsoleSender().sendMessage("[Sign-Manager] Locations loaded.");

    }

    public static Map<Location, String> getSigns() {
        return signs;
    }
}
