package me.imsanti.dev.managers;

import me.imsanti.dev.PotsRefill;
import me.imsanti.dev.managers.config.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class MessagesFile {

    private final PotsRefill potsRefill;

    public MessagesFile(ConfigManager configManager, PotsRefill potsRefill) {
        this.configManager = configManager;
        this.potsRefill = potsRefill;
    }

    private final ConfigManager configManager;

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(potsRefill.getDataFolder()), "messages")) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(potsRefill.getDataFolder()), "messages");
    }

    public File getConfigFile() {
        return new File(potsRefill.getDataFolder(), "messages.yml");
    }
}