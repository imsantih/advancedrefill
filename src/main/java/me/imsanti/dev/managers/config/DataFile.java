package me.imsanti.dev.managers.config;

import me.imsanti.dev.PotsRefill;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class DataFile {

    private final PotsRefill potsRefill;

    public DataFile(ConfigManager configManager, PotsRefill potsRefill) {
        this.configManager = configManager;
        this.potsRefill = potsRefill;
    }

    private final ConfigManager configManager;

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(potsRefill.getDataFolder()), "data")) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(potsRefill.getDataFolder()), "data");
    }

    public File getConfigFile() {
        return new File(potsRefill.getDataFolder(), "data.yml");
    }
}