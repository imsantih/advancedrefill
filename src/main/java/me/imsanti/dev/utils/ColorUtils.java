package me.imsanti.dev.utils;

import org.bukkit.ChatColor;

public class ColorUtils {

    public static String color(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }
}
