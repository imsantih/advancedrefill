package me.imsanti.dev.utils;

import org.bukkit.Material;

public class Utils {

    public static boolean isNumber(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

    public static boolean isMaterial(String material) {
        try {
            Material.valueOf(material);
            return true;
        }catch(IllegalArgumentException exception) {
            return false;
        }
    }
}
