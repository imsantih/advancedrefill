package me.imsanti.dev.utils;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.Material;

import java.util.HashSet;

public class PlayerUtils {

    public static boolean isLookingSign(Player player) {
        Location location = player.getTargetBlock((HashSet<Byte>) null, 1).getLocation();
        return location.getBlock().getType().equals(Material.WALL_SIGN) || location.getBlock().getType().equals(Material.SIGN_POST) || location.getBlock().getType().equals(Material.SIGN);
    }

}
