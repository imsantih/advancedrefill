package me.imsanti.dev.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class LocationUtils {

    public static void writeSign(Location location, ConfigurationSection section, int refillSize, String materialRefillName, boolean hasDataValue, int dataValue, int items) {
        section.set("x", (int) location.getX() + ".0");
        section.set("y", (int) location.getY() + ".0");
        section.set("z", (int) location.getZ() + ".0");
        section.set("yaw", location.getYaw());
        section.set("pitch", location.getPitch());
        section.set("world", location.getWorld().getName());

        section.set("refill-data.size", String.valueOf(refillSize));
        section.set("refill-data.material", String.valueOf(materialRefillName));
        section.set("refill-data.items-Amount", String.valueOf(items));
        if(hasDataValue) {
            section.set("refill-data.data-value", String.valueOf(dataValue));
        }

    }

    public static Location readLocation(ConfigurationSection section) {
        return new Location(Bukkit.getWorld(section.getString("world")), (int) Double.parseDouble(section.getString("x")), (int) Double.parseDouble(section.getString("y")), (int) Double.parseDouble(section.getString("z")));

    }
}
