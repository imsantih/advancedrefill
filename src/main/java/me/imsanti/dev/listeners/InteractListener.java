package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.RefillManager;
import me.imsanti.dev.managers.config.DataFile;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InteractListener implements Listener {

    private final RefillManager refillManagere;
    private final DataFile dataFile;

    public InteractListener(RefillManager refillManager, DataFile dataFile) {
        this.refillManagere = refillManager;
        this.dataFile = dataFile;
    }

    @EventHandler
    private void handleSignInteract(final PlayerInteractEvent event) {
        ItemStack itemStack;
        if(!(event.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
        if(!refillManagere.refillExists(event.getClickedBlock().getLocation())) return;

        ConfigurationSection sectionFirst = dataFile.getConfig().getConfigurationSection("signs." + RefillManager.getSigns().get(event.getClickedBlock().getLocation()));
        if(sectionFirst == null) return;

        if(sectionFirst.getString(".refill-data.data-value") != null) {
            itemStack = new ItemStack(Material.valueOf(sectionFirst.getString(".refill-data.material")), Integer.parseInt(sectionFirst.getString(".refill-data.items-Amount")), Short.parseShort(sectionFirst.getString(".refill-data.data-value")));
        }else {
            itemStack = new ItemStack(Material.valueOf(sectionFirst.getString(".refill-data.material")), Integer.parseInt(sectionFirst.getString(".refill-data.items-Amount")));

        }
        Inventory inventory = Bukkit.createInventory(null, Integer.parseInt(sectionFirst.getString(".refill-data.size")), "Refill");

        for(int i = 0; i < Integer.parseInt(sectionFirst.getString(".refill-data.size")); i++) {
            inventory.setItem(i, itemStack);
        }

        event.getPlayer().openInventory(inventory);

    }
}
