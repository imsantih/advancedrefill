package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.MessagesFile;
import me.imsanti.dev.managers.RefillManager;
import me.imsanti.dev.managers.config.DataFile;
import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.IOException;
import java.util.HashSet;

public class BreakListener implements Listener {

    private final RefillManager refillManager;
    private final DataFile dataFile;
    private final MessagesFile messagesFile;

    public BreakListener(RefillManager refillManager, DataFile dataFile, MessagesFile messagesFile) {
        this.refillManager = refillManager;
        this.dataFile = dataFile;
        this.messagesFile = messagesFile;
    }

    @EventHandler
    private void handlePluginSigns(final BlockBreakEvent event) {
        if(!event.getPlayer().hasPermission("refill.admin")) return;

        Location playerLocation = event.getPlayer().getTargetBlock((HashSet<Byte>) null, 1).getLocation();
        if(!refillManager.refillExists(playerLocation)) return;

        if(event.getPlayer().isSneaking()) {
            FileConfiguration config = dataFile.getConfig();
            for(final String key : dataFile.getConfig().getConfigurationSection("signs").getKeys(false)) {
                final ConfigurationSection section = config.getConfigurationSection("signs." + key);
                if(section.getString("x").equalsIgnoreCase(((int) event.getBlock().getLocation().getX() + ".0")) &&
                  section.getString("y").equalsIgnoreCase(( (int) event.getBlock().getLocation().getY() + ".0"))
                  && section.getString("z").equalsIgnoreCase(((int) event.getBlock().getLocation().getZ() + ".0"))) {
                    config.set("signs." + key, null);
                    try {
                        config.save(dataFile.getConfigFile());
                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }

                    event.getPlayer().sendMessage(ColorUtils.color(messagesFile.getConfig().getString("sign-removed")));
                    RefillManager.getSigns().remove(event.getBlock().getLocation());
                    return;
                }
            }
            return;
        }

        event.getPlayer().sendMessage(ColorUtils.color(messagesFile.getConfig().getString("cant-remove")));
        event.setCancelled(true);
    }

}
