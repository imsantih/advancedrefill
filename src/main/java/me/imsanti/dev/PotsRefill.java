package me.imsanti.dev;

import me.imsanti.dev.listeners.InteractListener;
import me.imsanti.dev.commands.RefillCommand;
import me.imsanti.dev.listeners.BreakListener;
import me.imsanti.dev.managers.MessagesFile;
import me.imsanti.dev.managers.RefillManager;
import me.imsanti.dev.managers.config.ConfigManager;
import me.imsanti.dev.managers.config.DataFile;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public final class PotsRefill extends JavaPlugin {

    private final ConfigManager configManager;
    private final DataFile dataFile;
    private final RefillManager refillManager;
    private final MessagesFile messagesFile;

    public PotsRefill() {
        this.configManager = new ConfigManager(this);
        this.dataFile = new DataFile(configManager, this);
        this.messagesFile = new MessagesFile(configManager, this);
        this.refillManager = new RefillManager(dataFile, messagesFile);

    }
    @Override
    public void onEnable() {
        createConfig();
        registerCommands();
        registerEvents();
        refillManager.loadRefills();
    }

    @Override
    public void onDisable() {
        RefillManager.getSigns().clear();
        HandlerList.unregisterAll();

    }


    private void createConfig() {
        if(dataFile.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created data.yml");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded data.yml");
        }

        if(messagesFile.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created messages.yml");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded messages.yml");
        }
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new BreakListener(refillManager, dataFile, messagesFile), this);
        getServer().getPluginManager().registerEvents(new InteractListener(refillManager, dataFile), this);

    }

    private void registerCommands() {
        this.getCommand("refill").setExecutor(new RefillCommand(refillManager, messagesFile));
    }

}
