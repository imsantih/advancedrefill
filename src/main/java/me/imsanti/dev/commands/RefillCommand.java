package me.imsanti.dev.commands;

import me.imsanti.dev.managers.MessagesFile;
import me.imsanti.dev.managers.RefillManager;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.UUID;

public class RefillCommand implements CommandExecutor {

    private final RefillManager refillManager;
    private final MessagesFile messagesFile;

    public RefillCommand(RefillManager refillManager, MessagesFile messagesFile) {
        this.refillManager = refillManager;
        this.messagesFile = messagesFile;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("no-console")));
            return true;
        }

        Player player = (Player) sender;
        if(args.length == 0) {
            sendHelp(player);
            return true;
        }
        if(player.hasPermission("refill.admin") || player.getUniqueId().equals("poneracaUUID")) {
            switch (args[0].toLowerCase()) {
                case "create":
                    String dataValue = "null";
                    if(args.length != 4) {
                        player.sendMessage(ColorUtils.color("&cCorrect usage: /refill create <material> <size> <stackAmount>"));
                        return true;
                    }

                    if(args[1].contains(":")) {
                        dataValue = args[1].split(":")[1];
                        args[1] = args[1].replace(dataValue, "").replaceAll(":", "");
                    }

                    if(!Utils.isMaterial(args[1].toUpperCase())) {
                        player.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("invalid-material").replaceAll("%material%", args[1].toUpperCase())));
                        return true;
                    }

                    if(!Utils.isNumber(args[2])) {
                        player.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("invalid-number").replaceAll("%number%", args[2])));
                        return true;
                    }

                    if(args[2].equalsIgnoreCase("9") || args[2].equalsIgnoreCase("18" ) || args[2].equalsIgnoreCase("27") || args[2].equalsIgnoreCase("36") || args[2].equalsIgnoreCase("45") || args[2].equalsIgnoreCase("54")) {
                        if(!Utils.isNumber(args[3])) {
                            player.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("invalid-number").replaceAll("%number%", args[2])));
                            return true;
                        }
                        if(!dataValue.equalsIgnoreCase("null")) {
                            refillManager.createRefill(player, UUID.randomUUID().toString(), Integer.parseInt(args[2]), args[1].toUpperCase(), true, Integer.parseInt(dataValue), Integer.parseInt(args[3]));

                        }else {
                            refillManager.createRefill(player, UUID.randomUUID().toString(), Integer.parseInt(args[2]), args[1].toUpperCase(), false, 0, Integer.parseInt(args[3]));

                        }
                    }else {
                        player.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("invalid-number").replaceAll("%number%", args[2])));
                    }

                    break;

                default:
                    sendHelp(player);
                    break;
            }
        }else {
            player.sendMessage(ColorUtils.color(messagesFile.getConfig().getString("no-permission")));
        }

        return true;
    }

    private void sendHelp(Player player) {
        player.sendMessage(ColorUtils.color("&8&m+---------------------------------+"));
        player.sendMessage(ColorUtils.color("       &6&lPots Refill"));
        player.sendMessage(ColorUtils.color("&e"));
        player.sendMessage(ColorUtils.color("&e/refill create <material> <size> <amount>"));
        player.sendMessage(ColorUtils.color("&e"));
        player.sendMessage(ColorUtils.color("&fDeveloped by &6santih#5851"));
        player.sendMessage(ColorUtils.color("&8&m+---------------------------------+"));
    }

}
